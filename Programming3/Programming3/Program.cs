﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog3_10004163
{
    class Program
    {
        static void Main(string[] args)
        {
          
            string pizzatype;
            int a;
            int b;
        
            var type = new List<string> { "Pepperoni", "Meat-Lovers", "Cheese", "Vegetarian", "Vegan","Garlic Prawn", "Peri-Peri Chicken", "Supreme", "Chicken And Cranberry", "Ham and Cheese", "Gluten Free" };
            Console.ForegroundColor = ConsoleColor.Yellow;
            List<Tuple<string, double>> pizza = new List<Tuple<string, double>>();
            Console.WriteLine("Please choose a size");
            pizza.Add(Tuple.Create("Kids Pizza", 5.00));
            pizza.Add(Tuple.Create("Regular Pizza", 9.00));
            pizza.Add(Tuple.Create("Large Pizza", 13.00));

            List<Tuple<string, double>> drinks = new List<Tuple<string, double>>();
            drinks.Add(Tuple.Create("Coke", 5.00));
            drinks.Add(Tuple.Create("Fanta", 5.00));
            drinks.Add(Tuple.Create("Lemonade", 5.00));
            drinks.Add(Tuple.Create("Iced-Tea", 4.00));

            List<Tuple<string, double>> dessert = new List<Tuple<string, double>>();
            dessert.Add(Tuple.Create("Cake", 5.00));
            dessert.Add(Tuple.Create("Fudge", 5.00));
            dessert.Add(Tuple.Create("Mirange", 6.00));
            dessert.Add(Tuple.Create("Ice-Cream", 8.00));

        Orderstart:
                        
            Console.Clear();
            Order.Screen();
            Console.WriteLine("Would you like to order Pizza or drinks?");
            Console.WriteLine("1. Order Drink");
            Console.WriteLine("2. Order Pizza");
            Console.WriteLine("3. Order Dessert");
            Console.WriteLine("4. Enter Your Details");
            Console.WriteLine("5. Complete Order");
            if (int.TryParse(Console.ReadLine(), out b))
            {
                switch (b)
                {
                    case 1:
                    drink:
                        Console.Clear();
                        Order.Screen();
                        for (a = 0; a < drinks.Count; a++)
                        {
                            Console.WriteLine($"{a + 1}. {drinks[a].Item1}");
                        }
                        Console.WriteLine($"{a + 1}. Go Back To Menu");

                        if (int.TryParse(Console.ReadLine(), out b))
                        {
                            if (b == a + 1)
                            {
                                goto Orderstart;
                            }
                            else if (b > a + 1)
                            {
                                Console.WriteLine("Error: Please  try again.");
                                Console.ReadKey();
                                goto drink;
                            }
                            else
                            {
                                Order.order.Add(Tuple.Create("Drinks", drinks[b - 1].Item1, drinks[b - 1].Item2));
                            }
                            goto drink;

                        }
                        else
                        {
                            Console.WriteLine("Error:Please try again.");
                            Console.ReadKey();
                            goto drink;
                        }
                    case 2:
                    pizza:
                        Console.Clear();

                        Console.WriteLine("Order:");
                        Order.Screen();
                        for (a = 0; a < type.Count; a++)
                        {
                            Console.WriteLine($"{a + 1}. {type[a]}");
                        }
                        Console.WriteLine($"{a + 1}. Go Back To Menu");

                        if (int.TryParse(Console.ReadLine(), out b))
                        {
                            if (b == a + 1)
                            {
                                goto Orderstart;
                            }
                            else if (b > a + 1)
                            {
                                Console.WriteLine("Error: Please try again.");
                                Console.ReadKey();
                                goto pizza;
                            }
                            else
                            {
                                pizzatype = type[b - 1];
                                Console.Clear();
                                Order.Screen();
                                Console.WriteLine($"Current Pizza:{pizzatype}");
                                Console.WriteLine("Choose the size you want!");
                                for (a = 0; a < pizza.Count; a++)
                                {
                                    Console.WriteLine($"{a + 1}. {pizza[a].Item1}");
                                }
                                Console.WriteLine($"{a + 1}. Go Back To Menu");

                                if (int.TryParse(Console.ReadLine(), out b))
                                {
                                    if (b == a + 1)
                                    {
                                        goto Orderstart;
                                    }
                                    else if (b > a + 1)
                                    {
                                        Console.WriteLine("Error: Please try again.");
                                        Console.ReadKey();
                                        goto pizza;
                                    }
                                    else
                                    {
                                        Order.order.Add(Tuple.Create(pizzatype, pizza[b - 1].Item1, pizza[b - 1].Item2));
                                    }
                                    goto pizza;
                                }
                                else
                                {
                                    Console.WriteLine("Error: Please try again.");
                                    Console.ReadKey();
                                    goto pizza;
                                }
                            }

                        }
                        else
                        {
                            Console.WriteLine("Error: Please try again.");
                            Console.ReadKey();
                            goto pizza;
                        }
                    case 3:
                    dessert:

                        Console.Clear();

                        Order.Screen();
                        for (a = 0; a < dessert.Count; a++)
                        {
                            Console.WriteLine($"{a + 1}. {dessert[a].Item1}");
                        }
                        Console.WriteLine($"{a + 1}. Go Back To Menu");

                        if (int.TryParse(Console.ReadLine(), out b))
                        {
                            if (b == a + 1)
                            {
                                goto Orderstart;
                            }
                            else if (b > a + 1)
                            {
                                Console.WriteLine("Error: Please  try again.");
                                Console.ReadKey();
                                goto dessert;
                            }
                            else
                            {
                                Order.order.Add(Tuple.Create("Dessert", dessert[b - 1].Item1, dessert[b - 1].Item2));
                            }
                            goto dessert;

                        }
                        else
                        {
                            Console.WriteLine("Error: Please  try again.");
                            Console.ReadKey();
                            goto dessert;
                        }
                    case 4:
                        Client.GetClientDetails();
                        goto Orderstart;
                      case 5:
                    Finalise:
                           if (Client.Number == 0)
                        {
                            Client.GetClientDetails();
                            goto Finalise;
                        }
                        else
                        {

                            Console.Clear();
                            Order.Screen();
                            Console.WriteLine("Is this what you wanted?");
                            Console.WriteLine("1. Yes");
                            Console.WriteLine("2. No");
                            if (int.TryParse(Console.ReadLine(), out b))
                            {
                                switch (b)
                                {
                                    case 1:
                                        Console.Clear();
                                        Console.WriteLine("We will call soon to confirm the order, Thank you for choosing Papa John's.");
                                        break;

                                    case 2:
                                        goto Orderstart;

                                }
                            }

                        }
                        break;

                    default:
                        Console.WriteLine("Error: Please  try again.");
                        Console.Read();
                        goto Orderstart;


                }
            }
            else
            {
                Console.WriteLine("Error: Please  try again.");
                Console.Read();
                goto Orderstart;
            }

        }
    }
    public static class Order
    {

        public static List<Tuple<string, string, double>> order = new List<Tuple<string, string, double>>();

        public static void Screen()
        {
            int i;
            double Price;
            string price;
            Price = 0;
            Console.WriteLine("Welcome to Papa Johns");
            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine($"Name: {Client.name}");
            Console.WriteLine($"Contact Number: {Client.displayNumber}");
            Console.WriteLine("Current Order:");
            for (i = 0; i < order.Count; i++)
            {
                price = String.Format("{0:C}", order[i].Item3);
                Console.WriteLine($"{order[i].Item1},{order[i].Item2}:{price}");
                Price = Price + order[i].Item3;
            }
            price = String.Format("{0:C}", Price);
            Console.WriteLine($"Price:{price}");
            Console.WriteLine("-----------------------------------------------------");
        }

    }
    
    public static class Client
    {
        public static int Number;
        public static string name;       
        public static string displayNumber;
        public static void GetClientDetails()
        {
            int b;
        Start:
            Console.Clear();
            Console.WriteLine("What is the name for the order?");
            name = Console.ReadLine();

            Console.WriteLine("What is a Contact Number we can use?");
            if (int.TryParse(Console.ReadLine(), out Number))
            {
                displayNumber = ($"{Number}");
                Console.WriteLine($"The name for the order is {name}");
                Console.WriteLine($"The Contact Number is {displayNumber}");

                Console.WriteLine("Are these the correct details");
                Console.WriteLine("1. Yes");
                Console.WriteLine("2. No");
                if (int.TryParse(Console.ReadLine(), out b))
                {
                    switch (b)
                    {
                        case 1:
                            break;

                        default:
                            goto Start;
                    }
                }
                else
                {
                    Console.WriteLine("Error: Please use Numbers only. Please  try again.");
                    Console.ReadKey();
                    goto Start;
                }

            }
            else
            {
                Console.WriteLine("Error: Please use Numbers only. Please  try again.");
                Console.ReadKey();
                goto Start;
            }


        }

    }

}
